module.exports = {
    productionSourceMap: false,
    //关闭eslint，关闭语法检查
    lintOnSave: false,

    //代理跨域
    devServer: {
        proxy: {
            '/api': {
                target: 'http://39.98.123.211',
                // pathRewrite: { '^/api': '' },
            },
        },
    },

}