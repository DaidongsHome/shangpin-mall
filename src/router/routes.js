//引入路由组件
// import Home from '@/pages/Home'
// import Search from '@/pages/Search'
// import Login from '@/pages/Login'
// import Register from '@/pages/Register'
// import Detail from '@/pages/Detail'
// import AddCartSuccess from '@/pages/AddCartSuccess'
// import ShopCart from '@/pages/ShopCart'
// import Trade from '@/pages/Trade'
// import Pay from '@/pages/Pay'
// import PaySuccess from '@/pages/PaySuccess'
// import Center from '@/pages/Center'
// //引入二级路由组件
// import MyOrder from '@/pages/Center/myOrder'
// import GroupOrder from '@/pages/Center/groupOrder'

//路由的配置信息
export default [
    {
        name: "center",
        path: "/center",
        component: () => import('@/pages/Center'),
        meta: {
            show: true
        },
        //二级路由组件
        children: [
            {
                path: 'myorder',
                component: () => import('@/pages/Center/myOrder'),
            },
            {
                path: 'grouporder',
                component: () => import('@/pages/Center/groupOrder'),
            },
            {//默认二级路由显示位置
                path: '/center',
                redirect: '/center/myorder',
            }
        ]
    },
    {
        name: "paysuccess",
        path: "/paysuccess",
        component: () => import('@/pages/PaySuccess'),
        meta: {
            show: true
        }
    },
    {
        name: "pay",
        path: "/pay",
        component: () => import('@/pages/Pay'),
        meta: {
            show: true
        },
        //路由独享守卫
        beforEnter: (to, from, next) => {
            //去支付页面，必须是从交易页面来
            if (from.path == '/trade') {
                next()
            } else {
                //从其他的路由组件而来的会停留在当前
                next(false)
            }
        }
    },
    {
        name: "trade",
        path: "/trade",
        component: () => import('@/pages/Trade'),
        meta: {
            show: true
        },
        //路由独享守卫
        beforEnter: (to, from, next) => {
            //去交易页面，必须是从购物车来
            if (from.path == '/shopcart') {
                next()
            } else {
                //从其他的路由组件而来的会停留在当前
                next(false)
            }
        }
    },
    {
        name: "shopcart",
        path: "/shopcart",
        component: () => import('@/pages/ShopCart'),
        meta: {
            show: true
        }
    },
    {
        name: "addcartsuccess",
        path: "/addcartsuccess",
        component: () => import('@/pages/AddCartSuccess'),
        meta: {
            show: true
        }
    },
    {
        name: "detail",
        path: "/detail/:skuid",
        component: () => import('@/pages/Detail'),
        meta: {
            show: true
        }
    },
    {
        name: "home",
        path: "/home",
        component: () => import('@/pages/Home'),
        meta: {
            show: true
        }
    },
    {
        name: "search",
        path: "/search/:keyword?",
        component: () => import('@/pages/Search'),
        meta: {
            show: true
        },
        //路由组件能不能传递props数据？
        //布尔值写法:只用于params
        // props: true,
        //对象写法:额外的给路由组件传递一些props
        // props: {
        //     a: 1,
        //     b: 2
        // },
        //函数写法：可以把params参数、query参数、通过props传递给路由组件
        props: ($route) => ({ keyword: $route.params.keyword, })
    },
    {
        path: "/login",
        component: () => import('@/pages/Login'),
        meta: {
            show: false
        }
    },
    {
        path: "/register",
        component: () => import('@/pages/Register'),
        meta: {
            show: false
        }
    },


    {
        // 重定向，在项目跑起来的时候，访问/立马让他定向到首页
        path: "*",
        redirect: "/home"
    }
]