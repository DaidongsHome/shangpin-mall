import Vue from 'vue'

import App from './App.vue'
//三级联动㢟--全局组件
import TypeNav from '@/components/TypeNav'
//Home轮播图--全局组件
import Carousel from '@/components/Carousel'
//分页器全局组件
import Pagination from '@/components/Pagination'
//ElenmentUI按需引入
import { Button, MessageBox } from 'element-ui';
//第一个参数：全局组件的名字 第二个参数：哪一个组件
Vue.component(TypeNav.name, TypeNav)
Vue.component(Carousel.name, Carousel)
Vue.component(Pagination.name, Pagination)
//注册全局组件
Vue.component(Button.name, Button)
//ElementUI注册组件的时候，还有一种写法：挂载原型上
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
//引入mockServer.js----mock虚拟数据
import '@/mock/mockServer.js'
//引入swiper样式
import 'swiper/css/swiper.min.css'
//统一接收api文件夹里面的全部请求函数
import * as API from '@/api'

//引入路由
import router from '@/router'
//引入仓库Vuex
import store from '@/store'
//引入懒加载插件
import VueLazyload from 'vue-lazyload'
//引入懒加载默认图片
import atm from '@/assets/atm.png'

Vue.config.productionTip = false

//注册插件
Vue.use(VueLazyload, {
  //懒加载默认图片
  loading: atm
})

//引入表单校验插件
import '@/plugins/validate'


new Vue({
  render: h => h(App),
  //配置全局事件总线$bus
  beforeCreate() {
    Vue.prototype.$bus = this;
    Vue.prototype.$API = API;
  },
  //注册路由
  router,
  //注册仓库：组件实例的身上会多一个属性$store属性
  store,
}).$mount('#app')
