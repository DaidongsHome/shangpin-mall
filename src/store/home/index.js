import { reCategoryList, reqGetBannerList, reqGetFloorList } from '@/api';
//home模块的小仓库
const state = {
    //state中数据默认值不能乱写，服务器返回的是对象，起始值就应该是对象，其余同理
    categoryList: [],
    //轮播图的数据
    bannerList: [],
    //floor的数据
    floorList: []
};
const mutations = {
    CATEGORYLIST(state, categoryList) {
        state.categoryList = categoryList.slice(0, 17);
    },
    GETBANNERLIST(state, bannerList) {
        state.bannerList = bannerList;
    },
    GETFLOORLIST(state, floorList) {
        state.floorList = floorList
    }
};
const actions = {
    //通过API里面的接口函数调用，向服务器发请求，获取服务器数据
    async categoryList({ commit }) {
        let result = await reCategoryList();
        if (result.code == 200) {
            //提交给mutations
            commit("CATEGORYLIST", result.data)
        }
        // console.log(result);
    },

    //获取首页轮播图的数据
    async getBannerList({ commit }) {
        let result = await reqGetBannerList();
        if (result.code == 200) {
            //提交给mutations
            commit('GETBANNERLIST', result.data)
        }
        // console.log(result);
    },

    //获取floor数据
    async getFloorList({ commit }) {
        let result = await reqGetFloorList();
        if (result.code == 200) {
            //提交给mutations
            commit('GETFLOORLIST', result.data)
        }
    }
};

const getters = {};

export default {
    state, mutations, actions, getters
}