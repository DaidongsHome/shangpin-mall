import { reqGetSearchInfo } from "@/api";
//search模块的小仓库
const state = {
    //仓库初始状态
    searchList: {}
};
const mutations = {
    GETSEARCHLIST(state, searchList) {
        state.searchList = searchList
    }
};
const actions = {
    //获取Search模块的数据
    async getSearchList({ commit }, params = {}) {
        //当前这个reqGetSearchInfo这个函数在调用服务器数据的时候，至少传递一个参数(空对象)
        //params参数：是用户派发action的时候，第二个参数传递过来的，至少是一个空对象
        let result = await reqGetSearchInfo(params)
        if (result.code == 200) {
            commit("GETSEARCHLIST", result.data)
        }
    }
};
//计算属性，在项目当中，为了简化数据而生。
//在项目当中gettes主要作用是：简化仓库中的数据(简化数据而生)
//可以把我们将来在组件当中需要用的数据简化一下【将来组件在获取数据的时候就方便了】
const getters = {
    //当前形参state是当前仓库中的state，并非打仓库中的那个state
    goodsList(state) {
        //这样书写是有问题的
        //如果服务器的数据回来的，则没有问题return回来的是一个数组
        //如果网络出问题或没有网络，则返回值就应该是undefined
        //所以新的计算属性的值至少给定义为一个数组
        return state.searchList.goodsList || []
    },
    trademarkList(state) {
        return state.searchList.trademarkList || []
    },
    attrsList(state) {
        return state.searchList.attrsList || []
    }
};

export default {
    state, mutations, actions, getters
}