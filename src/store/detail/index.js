import { reqGoodsInfo } from "@/api"
import { reqAddOrUppdateShopCart } from "@/api"
//封装游客身份的模块--生成一个随机字符串（不能改变）
import { getUuid } from '@/utils/uuid_token'
const state = {
    goodInfo: {},
    skuInfo: {},
    spuSaleAttrList: [],
    //游客的临时身份
    uuid_token: getUuid()
}
const mutations = {
    GETGOODINFO(state, goodInfo) {
        state.goodInfo = goodInfo
    }
}
const actions = {
    //获取产品信息的action
    async getGoodInfo({ commit }, skuId) {
        let result = await reqGoodsInfo(skuId)
        if (result.code == 200) {
            commit('GETGOODINFO', result.data)
        }
    },
    //将产品添加到购物车中||修改购物车中摸个产品的个数
    async addOrUppdateShopCart({ commit }, { skuId, skuNum }) {
        //加入购物车返回的结构
        //加入购物车以后（发请求），前台将参数带给服务器
        //服务器写入数据成功，并没诶呦返回其他的数据，至少返回code=200，代表这一次的操作成功
        //因为服务器没有返回其余的数据，因此我们不需要再进行三连环储存数据
        let result = await reqAddOrUppdateShopCart(skuId, skuNum)
        //当前的这个函数如果执行返回的是Promise
        if (result.code == 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('faile'))
        }
    }
}
//简化数据而生
const getters = {
    //路径导航简化的数据
    categoryView(state) {
        //比如：state.goodInfo初始状态是空对象，空对象的categoryView属性值是undefined
        //当前计算出的 categoryView属性值至少是一个空对象，这个假报错就没有了
        return state.goodInfo.categoryView || {};
    },
    //简化产品信息的数据
    skuInfo(state) {
        return state.goodInfo.skuInfo || {};
    },
    //产品售卖属性的简化
    spuSaleAttrList(state) {
        return state.goodInfo.spuSaleAttrList || []
    },
}
export default {
    state,
    mutations,
    actions,
    getters
}