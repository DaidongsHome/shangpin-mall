import { reqCartList } from "@/api"
import { reqDeleteCartById } from '@/api'
import { reqUpdateCheckById } from '@/api'
const state = {
    cartList: []
}
const mutations = {
    GETCARTLIST(state, cartList) {
        state.cartList = cartList
    }
}
const actions = {
    //获取购物车列表的数据
    async getCartList({ commit }) {
        let result = await reqCartList()
        if (result.code == 200) {
            commit("GETCARTLIST", result.data)
        }
    },
    //删除购物车某一个产品
    async deleteCartListBySkuId({ commit }, skuId) {
        let result = await reqDeleteCartById(skuId)
        if (result.code == 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('faile'))
        }
    },
    //修改购物车某一个产品的选中状态
    async reqUpdateCheckById({ commit }, { skuId, isChecked }) {
        let result = await reqUpdateCheckById(skuId, isChecked)
        if (result.code == 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('faile'))
        }
    },
    //删除全部勾选的产品[通过多次调用删除一个商品的操作来完成]
    deleteAllCheckedCart({ dispatch, getters }) {
        //context:小仓库，commit【提交mutations修改state】getters【计算属性】dispatch【派发action】state【当前仓库数据】
        //获取购物车中全部的产品
        let PromiseAll = []
        getters.cartList.cartInfoList.forEach(item => {
            let promise = item.isChecked == 1 ? dispatch('deleteCartListBySkuId', item.skuId) : ''
            //将每一次返回的Promise添加到数组当中
            PromiseAll.push(promise)
        })
        //只要全部的p1|p2|p3都成功，返回结果即为成功
        //如果有一个失败，则返回的即为失败结果
        return Promise.all(PromiseAll)
    },
    //修改购物车全部产品的状态
    updateAllCartIsChecked({ dispatch, state }, isChecked) {
        let promiseAll = []
        state.cartList[0].cartInfoList.forEach(item => {
            let promise = dispatch('reqUpdateCheckById', { skuId: item.skuId, isChecked })
            promiseAll.push(promise)
        })
        //最终返回的结果
        return PromiseAll.all(promiseAll)
    }
}
const getters = {
    cartList(state) {
        return state.cartList[0] || {}
    },
    //计算出来的购物车的数据
    // cartInfoList(state){
    //     return state.cartInfoList
    // }
}
export default {
    state, mutations, actions, getters
}