//当前这个模块：API进行同意管理
import requests from "./request";
import mockRequests from './mockAjax'

//三级联动的接口
///api/product/getBaseCategoryList  get 请求 无参数
//发请求；axios发请求返回的结果是Promise
export const reCategoryList = () => requests({
    url: '/product/getBaseCategoryList', method: 'get'
})

//获取banner（首页轮播图接口）
export const reqGetBannerList = () => mockRequests.get('/banner')

//获取floor数据
export const reqGetFloorList = () => mockRequests.get('/floor')

//获取搜索模块的数据 地址：/api/list 请求的房是:post 参数：需要带参数
//当前的这个函数需要接受外部传递的参数
//当前的这个结构，给服务器传递参数params，至少是一个空对象
//当前这个接口(获取搜索模块的数据)，给服务器传递一个默认参数【至少是一个空对象】
export const reqGetSearchInfo = (params) => requests({ url: '/list', method: "post", data: params })

//获取昌平详情信息的接口  /api/item/{ skuId }  get
export const reqGoodsInfo = (skuId) => requests({ url: `/item/${skuId}`, method: 'get' })

//将产品添加到购物车中（获取更新某一个产品的个数）
///api/cart/addToCart/{ skuId }/{ skuNum } POST
export const reqAddOrUppdateShopCart = (skuId, skuNum) => requests({ url: `/cart/addToCart/${skuId}/${skuNum}`, method: 'post' })

//获取购物车列表的数据结构
///api/cart/cartList get
export const reqCartList = () => requests({ url: '/cart/cartList', method: "get" })

//删除购物车产品的接口
///api/cart/deleteCart/{skuId} delete
export const reqDeleteCartById = (skuId) => requests({ url: `/cart/deleteCart/${skuId}`, method: 'delete' })

//修改购物车商品选中状态
///api/cart/checkCart/{skuID}/{isChecked} get
export const reqUpdateCheckById = (skuID, isChecked) => requests({ url: `/cart/checkCart/${skuID}/${isChecked}`, method: 'get' })

//获取注册手机验证码
//url:/api/user/passport/sendCode/{phone}  get
export const reqGetCode = (phone) => requests({ url: `/user/passport/sendCode/${phone}`, method: 'get' })

//注册的接口
//url：/api/user/passport/register post
export const reqUserRegister = (data) => requests({ url: '/user/passport/register', data, method: 'post' })

//登录的接口
///api/user/passport/login  post
export const reqUserLogin = (data) => requests({ url: '/user/passport/login', data, method: 'post' })

//获取用户信息【token】
// /api/user/passport/auth/getUserInfo get
export const reqUserInfo = () => requests({ url: '/user/passport/auth/getUserInfo', method: 'get' })

//退出登录
// /api/user/passport/logout get
export const reqLogOut = () => requests({ url: '/user/passport/logout', method: 'get' })

//获取用户地址信息
// /api/user/userAddress/auth/findUserAddressList  get
export const reqAddressInfo = () => requests({ url: '/user/userAddress/auth/findUserAddressList', method: 'get' })

//获取订单交易页商品信息
// /api/order/auth/trade get
export const reqOrderInfo = () => requests({ url: '/order/auth/trade', method: 'get' })

//提交订单的接口
// /api/order/auth/submitOrder?tradeNo={tradeNo} post
export const reqSubmitOrder = (tradeNo, data) => requests({ url: `/order/auth/submitOrder?tradeNo=${tradeNo}`, data, method: 'post' })

//获取支付信息
// /api/payment/weixin/createNative/{orderId} get
export const reqPayInfo = (orderId) => requests({ url: `/payment/weixin/createNative/${orderId}`, method: 'get' })

//获取支付订单状态信息
// /api/payment/weixin/queryPayStatus/{orderId} get
export const reqPayStatus = (orderId) => requests({ url: `/payment/weixin/queryPayStatus/${orderId}`, method: 'get' })

//获取个人中心的数据
// /api/order/auth/{page}/{limit} get
export const reqMyOrderList = (page, limit) => requests({ url: `/order/auth/${page}/${limit}`, method: 'get' })