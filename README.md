### 基于vue2.0的线上商城项目

#### 介绍
完成商城网站的页面布局和功能设计。调用相应的后端数据接口，实现用户注册登录、网站导航、商品内容展示、商品选购、购物车添加与删除、支付结算等功能。

#### 使用技术
1. 使用html、css、JavaScript配置页面内容
2. 使用vuex、vueRoute、$bus等实现数据储存和组件通信
3. 使用axios、mockServe动态接收数据和模拟虚拟数据
4. 使用swiper、uuid、ElementUI等组件库开发相应页面功能，并手写封装了Pagination分页器组件
5. 使用xshell、linux、宝塔等工具部署项目上线到云服务器

